# CoolMovie - Challenge



## Getting started



```
git clone git@gitlab.com:devguusta/coolmovie.git
git checkout origin main
flutter clean && flutter pub get
```


***

# More informations

For more informations about how to run the project and to start the backend, please access the following link:

- https://gitlab.com/ecoPortal/coolmovies/-/tree/main/

## Social Media
 - Linkedin: https://www.linkedin.com/in/gustavo-silva-8002201b5/

