import 'package:coolmovies_challenge/core/routes_delegate.dart';
import 'package:coolmovies_challenge/features/movies/core/inject/movie_inject.dart';
import 'package:coolmovies_challenge/features/movies/presenter/cubit/movie_cubit.dart';

import 'package:coolmovies_ds/external/themes/app_normal_theme.dart';
import 'package:core/core.dart';

import 'package:flutter/material.dart';

void main() async {
  await CoreInject.initialize();

  await MovieInject.initialize();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MovieCubit(
      getMoviesUsecase: getIt(),
          createReviewUseCase: getIt(),
          updateReviewUsecase: getIt()
        ),
      child: MaterialApp(
        initialRoute: CoolMoviesRoutes.splash,
        routes: CoolMoviesRoutesDelegate.managerRoutes,
        theme: AppNormalTheme.theme,
      ),
    );
  }
}
