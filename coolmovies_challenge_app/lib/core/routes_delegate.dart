import 'package:coolmovies_challenge/features/auth/login/presenter/login_page.dart';
import 'package:coolmovies_challenge/features/auth/splash/presenter/splash_page.dart';
import 'package:coolmovies_challenge/features/movies/presenter/pages/movies/movie_create_edit_review_page.dart';
import 'package:coolmovies_challenge/features/movies/presenter/pages/movies/movies_detail_page.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';

import '../features/movies/presenter/pages/home/home_page.dart';

class CoolMoviesRoutesDelegate {
  static Map<String, Widget Function(BuildContext)> managerRoutes = {
    CoolMoviesRoutes.splash: (context) => const SplashPage(),
    CoolMoviesRoutes.login: (context) => const LoginPage(),
    CoolMoviesRoutes.home: (context) => const HomePage(),

         CoolMoviesRoutes.detailsMovie: (context) => const  MoviesDetailPage(),
            CoolMoviesRoutes.createOrEditReview: (context) => const  CreateEditReviewMoviePage(),
        
  };
}
