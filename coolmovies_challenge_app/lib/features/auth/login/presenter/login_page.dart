import 'package:coolmovies_challenge/features/auth/login/presenter/widgets/profile_list_options.dart';
import 'package:coolmovies_challenge/features/auth/login/presenter/widgets/profile_widget.dart';
import 'package:coolmovies_ds/external/styles/text_styles.dart';
import 'package:core/core.dart';

import 'package:flutter/material.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
return Scaffold(
      appBar: AppBar(),
      body: Column(
      
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.12,),
              Text("Who's Watching ?", style: AppTextStyles.bodyLarge),
            ...ProfileListOptions.profilesName.map((e) {
              return Padding(
                padding: const EdgeInsets.only(right: 24.0, top: 40),
                child: GestureDetector(
                  onTap: (){
                    Navigator.pushReplacementNamed(context, CoolMoviesRoutes.home);
                  },
                  child: ProfileWidget(name: e,),),
              );
              
                
            })
          
      ],)
    
    );
  }
}