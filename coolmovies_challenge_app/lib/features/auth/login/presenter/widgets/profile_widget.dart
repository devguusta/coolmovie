// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:coolmovies_ds/external/styles/text_styles.dart';
import 'package:flutter/material.dart';

class ProfileWidget extends StatelessWidget {
  final String name;
  const ProfileWidget({
    Key? key,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: const BoxDecoration(
     image: DecorationImage(
        image: AssetImage(
            'assets/images/profile.png'),
        fit: BoxFit.fill,
      ),
      shape: BoxShape.rectangle,
                      ),
                    ),
                    const SizedBox(height: 8,),
                    Text(name, style: AppTextStyles.bodyLarge),
                  ],
                )
              ] 
            );
  }
}
