// ignore_for_file: unnecessary_import

import 'package:coolmovies_challenge/features/movies/core/enums/action_type.dart';
import 'package:coolmovies_challenge/features/movies/presenter/cubit/movie_cubit.dart';
import 'package:coolmovies_ds/external/colors/app_colors.dart';
import 'package:coolmovies_ds/external/external.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../domain/entities/entities.dart';
import '../../cubit/movie_state.dart';

class CreateEditReviewMoviePage extends StatefulWidget {
  const CreateEditReviewMoviePage({super.key});

  @override
  State<CreateEditReviewMoviePage> createState() =>
      _CreateEditReviewMoviePageState();
}

class _CreateEditReviewMoviePageState extends State<CreateEditReviewMoviePage> {
  late final TextEditingController bodyTextEditingController;
  late final TextEditingController ratingTextEditingController;
  late final TextEditingController titleTextEditingController;
  final _formKey = GlobalKey<FormState>();
  late final MovieCubit cubit;
  TextInputFormatter numericOnlyFormatter = FilteringTextInputFormatter.allow(RegExp(r'[0-5]'));
  int quantityBuild= 0;
  @override
  void initState() {
    
    super.initState();
    cubit = context.read<MovieCubit>();
    bodyTextEditingController = TextEditingController();
    ratingTextEditingController = TextEditingController();
    titleTextEditingController = TextEditingController();
  }

  @override
  void dispose() {
    bodyTextEditingController.dispose();
    ratingTextEditingController.dispose();
    titleTextEditingController.dispose();
    super.dispose();
  }

  void handleCreateUpdateReviewer(Movie movie, ActionType type, MovieReview? review) {
  
    FocusManager.instance.primaryFocus?.unfocus();
    if (_formKey.currentState?.validate() ?? false) {
      if(type == ActionType.update){
          final MovieReview movieReview = MovieReview(
      title: titleTextEditingController.text,
      body: bodyTextEditingController.text,
      id: review!.id,
      userReviewerId: review.userReviewerId,
      rating: int.parse(ratingTextEditingController.text),
      user: movie.userCreator!,
    );
     cubit.updateMovieReview(movieReview);
      } else {
         final MovieReview movieReview = MovieReview(
          userReviewerId: review?.userReviewerId ?? movie.userCreator!.id,
      title: titleTextEditingController.text,
      body: bodyTextEditingController.text,
      id: movie.id,
      rating: int.parse(ratingTextEditingController.text),
      user: movie.userCreator!,
    );
     cubit.createMovieReview(movieReview);
      }
     
    } else {
      _formKey.currentState?.reset();
    }
  }

  void _showDialog(Widget widget) async {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: widget,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    final movie = arguments['movie'] as Movie;
     final movieReview = arguments['movieReview'] as MovieReview?;
    final ActionType type = arguments['actionType'] as ActionType;
    if(type == ActionType.update && quantityBuild == 0){
       bodyTextEditingController.text = movieReview!.body;
       titleTextEditingController.text = movieReview.title;
       ratingTextEditingController.text = movieReview.rating.toString();
       quantityBuild++;
    }

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: SafeArea(
        child: BlocListener<MovieCubit, MovieState>(
          listener: (context, state)async{
           
            if(state is MovieStateFailure){
            
              _showDialog(Column( 
                mainAxisSize: MainAxisSize.min,
                children: [
                  const Text("It is not possible to create a comment now, please try again"),
                  CustomPrimaryButton(onPressed: (){

                    Navigator.pop(context);
                  }, text: "OK")
                ],
              ));
            }
            if(state is MovieStateCreated){
            
             await cubit.getMovies();
              
            }
            if(state is MovieStateLoaded){
            
              cubit.setMovie(movies: state.movies, movie: movie);
            
              Navigator.popUntil(context,(route) => route.settings.name == CoolMoviesRoutes.detailsMovie,);
            
            
             
            }
          
          },
          child: Scaffold(
            appBar: AppBar(
              actions: [
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.search,
                      color: AppColors.white,
                    )),
              ],
            ),
            body: SingleChildScrollView(
              child: SizedBox(
                height: MediaQuery.of(context).size.height - 72,
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Image.asset("assets/images/stream_flix_logo.png", width: 50, height: 50,),
                          const SizedBox(height: 16,),
                CustomTextFormField(
                        hintText: "Title",
                        controller: titleTextEditingController,
                        validator: (value) => ValidatorUtils.requiredField(value),
                        textInputAction: TextInputAction.next,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      CustomTextFormField(
                        hintText: "Rating",
                        controller: ratingTextEditingController,
                        textInputType: TextInputType.number,
                        validator: (value) => ValidatorUtils.requiredField(value),
                        textInputAction: TextInputAction.next,
                        inputFormatters: [
                          numericOnlyFormatter
                        ],
                       
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      SizedBox(
                      
                        child: CustomTextFormField(
                          hintText: "Body",
                          controller: bodyTextEditingController,
                          textInputType: TextInputType.multiline,
                          textInputAction: TextInputAction.done,
                          validator: (value) =>
                              ValidatorUtils.requiredField(value),
                        ),
                      ),
                        ],
                      ),
                  
                      CustomPrimaryButton(
                        onPressed: () => handleCreateUpdateReviewer(movie, type, movieReview),
                        text: type == ActionType.create ? "Create review" : "Update Review",
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
