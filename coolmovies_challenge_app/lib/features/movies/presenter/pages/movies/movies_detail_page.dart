// ignore_for_file: unused_import, unnecessary_cast

import 'package:coolmovies_challenge/features/movies/core/enums/action_type.dart';
import 'package:coolmovies_challenge/features/movies/domain/entities/movie.dart';
import 'package:coolmovies_challenge/features/movies/domain/entities/movie_review.dart';
import 'package:coolmovies_challenge/features/movies/presenter/cubit/movie_cubit.dart';
import 'package:coolmovies_ds/coolmovies_ds.dart';
import 'package:coolmovies_ds/external/styles/text_styles.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';

import '../../cubit/movie_state.dart';

class MoviesDetailPage extends StatefulWidget {
  const MoviesDetailPage({super.key});

  @override
  State<MoviesDetailPage> createState() => _MoviesDetailPageState();
}

class _MoviesDetailPageState extends State<MoviesDetailPage> {
  late MovieCubit cubit;


  @override
  void initState() {
    
    super.initState();
    cubit = context.read<MovieCubit>();
  }
  
  @override
  Widget build(BuildContext context) {
     final movie =
        ModalRoute.of(context)!.settings.arguments as Movie;
     cubit.setMovie(movie: movie);
     
        var snackBar = SnackBar(
          backgroundColor: AppColors.grey,
          showCloseIcon: true,          
  content: Text('In development', style: AppTextStyles.bodySmall,),
);



    return  SafeArea(
      
   
      child: Scaffold(
        appBar: AppBar(
          
          actions: [
            IconButton(onPressed: (){}, icon: Icon(Icons.search,color: AppColors.white,)),
          ],
        ),
        body: BlocBuilder<MovieCubit, MovieState>(
          builder: (context, state) {
            if(state is MovieStateLoaded){
       return  SizedBox(
          height: MediaQuery.of(context).size.height - 72,
           child: SingleChildScrollView(
                     physics: const ScrollPhysics(),
                    child: Column(children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Icon(
                           Icons.play_arrow, color: AppColors.white, size: 120,),
                      
                          CardMovieWidget(imageUrl: (state as MovieStateLoaded).currentMovie!.imageUrl, size: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height * 0.5)),
                       
                         
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                              Image.asset(
                          "assets/images/stream_flix_logo.png",
                          height: 30,
                          width: 30,
                        ),
                            Text("MOVIE", style: AppTextStyles.bodyMedium,)
                          ],
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(state.currentMovie!.title, style: AppTextStyles.title.copyWith(fontSize: 30),),
                          const SizedBox(height: 8,),
                          
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SizedBox(
                                  width: MediaQuery.sizeOf(context).width * 0.45,
                                  child: CustomPrimaryButton(
                                      
                                      onPressed: ()async{
                                    
                                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                      },
                                      text: "Download",
                                    ),
                                ),
                                const SizedBox(width: 4,),
                                TextButton(onPressed: (){
                                Navigator.pushNamed(context, CoolMoviesRoutes.createOrEditReview,
                                
                                arguments: {
                                  'movie': movie,
                                  'actionType': ActionType.create,
                                  "movieReview":  (state).currentMovie!.moviewReview != null && (state).currentMovie!.moviewReview!.isNotEmpty ?  (state).currentMovie!.moviewReview!.first : null,
                                  
                                },
                                );
                                },style: TextButton.styleFrom(
                    foregroundColor: AppColors.white,
                  ), child: const Text("Add your review"),),
                                
                              ],
                            ),
                            Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                    Text("Release: ", style: AppTextStyles.bodySmall,),
                              Text(state.currentMovie!.releaseDate, style: AppTextStyles.bodySmall,),
                                ],
                              ),
            
                                Row(
                                children: [
                                   Text("Autor: ", style: AppTextStyles.bodySmall,),
                              Text((state).currentMovie!.userCreator!.name, style: AppTextStyles.bodySmall,),
                                ],
                              ),
                          
                             
                            ],
                          ),
                          const SizedBox(height: 12,),
                          SizedBox(
                            
                            child: ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                              itemCount: (state).currentMovie!.moviewReview!.length,
                              itemBuilder: (context, index) => ListTile(
                                leading:  Image.asset(
                            "assets/images/Profile-ring-small.png",
                            height: 70,
                            width: 70,
                                      ),
                                      title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            children: [
                                              Text  ("${(state).currentMovie!.moviewReview![index].user.name} - ", style: AppTextStyles.bodySmall.copyWith(fontSize:  12),),
                                               Text((state).currentMovie!.moviewReview![index].title, style:AppTextStyles.bodySmall.copyWith(fontSize:  12),),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                            
                                              IconButton(onPressed: (){
         
         Navigator.pushNamed(context, CoolMoviesRoutes.createOrEditReview,
                                
                                arguments: {
                                  'movie': movie,
                                  'actionType': ActionType.update,
                                  'movieReview':(state).currentMovie!.moviewReview![index]
                                },
                                );
         
                                              }, icon:Icon(Icons.edit, color: AppColors.white, size:  15,))
                                            ],
                                          ),
                                          
                                          
                                        ],
                                      ),
                                      subtitle: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text((state).currentMovie!.moviewReview![index].body, style: AppTextStyles.bodySmall.copyWith(fontSize: 12,),),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                               FittedBox(
                                                
                                                child: Row(
                                                  children: List.generate( (state).currentMovie!.moviewReview![index].rating  >=5 ? 5 :  (state).currentMovie!.moviewReview![index].rating.toInt(), (index) => const Icon(Icons.star, color: Colors.yellow,size: 15,),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                          
                            )),
                          ),
                        
                            
                        ],
                      ),
                    )
                   
                    ],),
                  ),

       );
            } else {
              
            }
      return const Center(child: CircularProgressIndicator.adaptive());
          },
        ),
      ),
    );
  }
}