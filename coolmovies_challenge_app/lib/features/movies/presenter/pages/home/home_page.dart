import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';
import 'package:coolmovies_challenge/features/movies/presenter/cubit/movie_cubit.dart';
import 'package:coolmovies_challenge/features/movies/presenter/cubit/movie_state.dart';
import 'package:coolmovies_ds/coolmovies_ds.dart';
import 'package:coolmovies_ds/external/styles/text_styles.dart';
import 'package:core/core.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  late final MovieCubit cubit;
   late AnimationController _controller;
   late Animation _contentAnimation;
   late Animation _profilePictureAnimation;
  late Animation _listAnimation;
 

  @override
  void initState() {
    super.initState();
    cubit = context.read<MovieCubit>()..getMovies();
    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 4));
    _profilePictureAnimation = Tween(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: _controller,
            curve: const Interval(0.0, 0.20, curve: Curves.easeOut)));
    _contentAnimation = Tween(begin: 0.0, end: 22.0).animate(CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.20, 0.40, curve: Curves.easeOut)));
    _listAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: const Interval(0.40, 0.75, curve: Curves.easeOut)));
   
    _controller.forward();
    _controller.addListener(() {
      
    });
   
  }
  @override
  void dispose() {
   cubit.close();
   _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: const Icon(Icons.home),
            label: 'Home',
            backgroundColor: AppColors.black,
           
         
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Watchlist',
           
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'Library',
            
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Downloads',
           
          ),
        ],
        currentIndex: 0,
        selectedItemColor: AppColors.white,
        unselectedItemColor: AppColors.grey,
        // onTap: _onItemTapped,
      ),
      
        body: BlocBuilder<MovieCubit, MovieState>(
          bloc: cubit,
          builder: (context, state) {
            
            if(state is MovieStateFailure){
              return Column(
                children: [
                  Center(
                    child: Text("Server unavailable", style: AppTextStyles.bodyMedium,),


                  ),
                  CustomPrimaryButton(onPressed: ()async{
                  await  cubit.getMovies();
                  }, text: "Try again")
                ],
              );
            }
            if(state is MovieStateLoaded){
               return SingleChildScrollView(
              child: AnimatedBuilder(
                animation: _controller,
                builder: (context, _) {
                  return Column(
                    children: [
                   Opacity(
                    opacity: _profilePictureAnimation.value,
                    child: GestureDetector(
                      onTap: _profilePictureAnimation.value == 1 ?(){
                     
                        Navigator.pushNamed(context, CoolMoviesRoutes.detailsMovie, arguments:  (state.movies.first));
                      } : (){},
                      child: CardMovieWidget(imageUrl: state.movies.first.imageUrl,
                      body:    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                        Image.asset(
            "assets/images/stream_flix_logo.png",
            height: 45,
            width: 100,
          ),
           Image.asset(
              "assets/images/Profile-ring-small.png",
              height: 70,
              width: 70,
            )
                      ],),
                      ))),
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all( 8.0),
                            child: Row(
                              children: [
                                Text("Trending Now", style: AppTextStyles.bodyMedium.copyWith(fontSize: _contentAnimation.value,),),
                              ],
                            ),
                          ),
                               Opacity(
                                opacity: _listAnimation.value,
                                child: CardMoviesCarousel(movies: state.movies)),
                         
                          
                        ],
                      ),
                      
                    ],
                  );
                }
              ),
            );
            }
            return  SizedBox(
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Column(children: [
                 const ShimmerLoading( child: CardMovieWidget(
                  imageUrl:  'https://flutter'
                '.dev/docs/cookbook/img-files/effects/split-check/Food1.jpg',
                          
                 ),),
                 Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all( 8.0),
                            child: Row(
                              children: [
                                ShimmerLoading(child: Text("Trending Now", style: AppTextStyles.bodyMedium,)),
                              ],
                            ),
                          ),
                               ShimmerLoading(
                                 child: CardMoviesCarousel(movies: [
                                  Movie(
                                 
                                    id: "1", imageUrl: 'https://flutter'
                                           '.dev/docs/cookbook/img-files/effects/split-check/Food1.jpg', title: "title", releaseDate: "releaseDate")
                                 ]),
                               ),
                         
                          
                        ],
                      ),
                ],),
              ),
            );
          },
        ),
      ),
    );
  }
}
