import 'package:core/core.dart';

import '../../domain/entities/entities.dart';

abstract class MovieState extends Equatable{}


class MovieStateInitial extends MovieState{
  @override
 
  List<Object?> get props => [];
}

class MovieStateLoading extends MovieState{
  @override

  List<Object?> get props => [];
}

class MovieStateLoaded extends MovieState {
  final List<Movie> movies;
  final Movie? currentMovie;
  final int timesSetMovieWasCallback;
  MovieStateLoaded({
    required this.movies,
    this.currentMovie,
    this.timesSetMovieWasCallback = 0,
  });
  @override

  List<Object?> get props => [movies, currentMovie,currentMovie];  

  MovieStateLoaded copyWith({
    List<Movie>? movies,
    Movie? currentMovie,
    int? timesSetMovieWasCallback,
  }) {
    return MovieStateLoaded(
      movies: movies ?? this.movies,
      currentMovie: currentMovie ?? this.currentMovie,
      timesSetMovieWasCallback: timesSetMovieWasCallback ?? this.timesSetMovieWasCallback
    );
  }
}
class MovieStateCreated extends MovieState{
  @override

  List<Object?> get props => [];
}
class MovieStateFailure extends MovieState {
  final Failure failure;
  MovieStateFailure({
    required this.failure,
  });
  @override

  List<Object?> get props => [failure];
}
