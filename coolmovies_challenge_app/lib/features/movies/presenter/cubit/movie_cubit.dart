import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';
import 'package:coolmovies_challenge/features/movies/domain/usecases/create_reviewer_movie.dart';
import 'package:coolmovies_challenge/features/movies/domain/usecases/get_movies.dart';
import 'package:coolmovies_challenge/features/movies/domain/usecases/update_review_usecase.dart';
import 'package:coolmovies_challenge/features/movies/presenter/cubit/movie_state.dart';

import 'package:core/core.dart';

class MovieCubit extends Cubit<MovieState> {
  final GetMoviesUsecase getMoviesUsecase;
  final CreateReviewUseCase createReviewUseCase;
  final UpdateReviewUsecase updateReviewUsecase;
  MovieCubit({
    required this.getMoviesUsecase,
    required this.createReviewUseCase,
    required this.updateReviewUsecase,
  }) : super(MovieStateInitial());

  Future<void> getMovies()async{
    emit(MovieStateLoading());
  
    final result = await getMoviesUsecase(NoParams());
    result.fold((l) => emit(MovieStateFailure(failure: l)), (r) => emit(MovieStateLoaded(movies: r)));
  }


  void setMovie({ List<Movie>? movies,required Movie movie}){
    final currentState = (state as MovieStateLoaded);
    if(currentState.timesSetMovieWasCallback == 0){
        emit(currentState.copyWith(
      timesSetMovieWasCallback: 1,
      currentMovie: movies?.firstWhere((element) => element.id == movie.id) ?? movie),
      );
    }
 
    
  }
  

   Future<void> createMovieReview(MovieReview movieReview)async{
    emit(MovieStateLoading());
  
    final result = await createReviewUseCase(movieReview);
    result.fold((l) => emit(MovieStateFailure(failure: l)), (r) => emit(MovieStateCreated()));
  }
  

    Future<void> updateMovieReview(MovieReview movieReview)async{
    emit(MovieStateLoading());
  
    final result = await updateReviewUsecase(movieReview);
    result.fold((l) => emit(MovieStateFailure(failure: l)), (r) => emit(MovieStateCreated()));
  }
  
}
