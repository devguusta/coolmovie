import 'package:coolmovies_challenge/features/movies/data/models/movie_review_model.dart';

import '../../domain/entities/entities.dart';

abstract class MovieDatasource{
  Future<List<Movie>> getMovies();
  Future<bool> createReview(MovieReviewModel review);
    Future<bool> editReview(MovieReviewModel review);
}