import 'package:coolmovies_challenge/features/movies/core/movie_requests.dart';
import 'package:coolmovies_challenge/features/movies/data/models/movie_model.dart';
import 'package:coolmovies_challenge/features/movies/data/models/movie_review_model.dart';
import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';
import 'package:coolmovies_challenge/features/movies/domain/entities/movie.dart';

import 'package:core/external/data/data_service.dart';
import 'package:core/external/external.dart';
import 'package:flutter/material.dart';

import 'movie_datasource.dart';

final class MovieDatasourceImpl implements MovieDatasource {
  final DataService _dataService;
  MovieDatasourceImpl({
    required DataService dataService
  }): _dataService = dataService;
  @override
  Future<List<Movie>> getMovies() async{
    
   try {
    List<Movie> movies = [];
     final result = await _dataService.read(path: MovieRequests.allMoviesPath);
     if(result != null  && (result as Map ).containsKey("allMovies")){
      movies = (result['allMovies']['nodes'] as List).map((e) => MovieModel.fromMap(e)).toList();
      return movies; 

     }
     return [];
   } catch (e, stacktrace) {
    debugPrint(e.toString() + stacktrace.toString());
     throw const ServerFailure(message: "Erro ao buscar os filmes");
   }
  }
  
  @override
  Future<bool> createReview(MovieReviewModel movieReview)async {
     try {
 
      await _dataService.create(path: MovieRequests.createMovieReviewMutation,
     params: movieReview.toMap(),
     );
    
     return true; 
   } catch (e, stacktrace) {
    debugPrint(e.toString() + stacktrace.toString());
     throw const ServerFailure(message: "Error to create review");
   }
  }
  
  @override
  Future<bool> editReview(MovieReviewModel review) async{
     try {
 
 await _dataService.update(path: MovieRequests.updateMovieReviewPath,
     params: review.toUpdateMap(),
     );
    
     return true; 
   } catch (e, stacktrace) {
    debugPrint(e.toString() + stacktrace.toString());
     throw const ServerFailure(message: "Erro to update a review");
   }
  }
  
}
