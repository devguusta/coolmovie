import 'dart:convert';

import 'package:coolmovies_challenge/features/movies/domain/entities/user_reviewer.dart';

class UserModel extends User{
  UserModel({required super.id, required super.name});

    factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
    );
  }



  factory UserModel.fromJson(String source) => UserModel.fromMap(json.decode(source));
}