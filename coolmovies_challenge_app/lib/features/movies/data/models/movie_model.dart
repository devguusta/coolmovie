import 'dart:convert';

import 'package:coolmovies_challenge/features/movies/data/models/movie_review_model.dart';
import 'package:coolmovies_challenge/features/movies/data/models/user_reviewer_model.dart';

import '../../domain/entities/entities.dart';

class MovieModel extends Movie{
  MovieModel({required super.id, required super.imageUrl, super.moviewReview, required super.title, required super.releaseDate,  super.userCreator, });


   factory MovieModel.fromMap(Map<String, dynamic> map) {
    return MovieModel(
    
      id: map['id'] ?? '',
      imageUrl: map['imgUrl'] ?? '',
      title: map['title'] ?? '',
      releaseDate: map['releaseDate'] ?? '',
      moviewReview:map['movieReviewsByMovieId']['nodes'] != null ? List<MovieReviewModel>.from(map['movieReviewsByMovieId']['nodes']!.map((x) => MovieReviewModel.fromMap(x))) : [],
  userCreator: UserModel.fromMap(map['userByUserCreatorId']
      ),
    );
  }

    factory MovieModel.fromJson(String source) => MovieModel.fromMap(json.decode(source));
}