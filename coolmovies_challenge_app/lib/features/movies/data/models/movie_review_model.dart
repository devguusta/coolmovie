import 'dart:convert';

import 'package:coolmovies_challenge/features/movies/data/models/user_reviewer_model.dart';
import 'package:coolmovies_challenge/features/movies/domain/entities/movie_review.dart';

class MovieReviewModel extends MovieReview{
  MovieReviewModel({required super.body, required super.id, required super.title, required super.user, required super.rating, required super.userReviewerId});

factory MovieReviewModel.fromMap(Map<String, dynamic> map) {
    return MovieReviewModel(
      userReviewerId: map['userReviewerId'],
      rating: map['rating'] ?? 0,
      body: map['body'] ?? '',
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      user: UserModel.fromMap(map['userByUserReviewerId']),
    );
  }


  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{
          'title': title,
          'movieId': id,
          'userReviewerId': userReviewerId,
          'rating': rating,
          'body': body,
          'name': user.name
        
    };
  
    return result;
  }


  Map<String, dynamic> toUpdateMap() {
    final result = <String, dynamic>{
      'body': body,
      'rating': rating,
      'title': title,
      'id': id,
    };
    return result;
  }
  

    String toJson() => json.encode(toMap());

  factory MovieReviewModel.fromJson(String source) => MovieReviewModel.fromMap(json.decode(source));
}