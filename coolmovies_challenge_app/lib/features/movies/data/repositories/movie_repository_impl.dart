import 'package:coolmovies_challenge/features/movies/data/models/movie_review_model.dart';
import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';


import 'package:core/core.dart';

import '../../domain/repositories/movie_repository.dart';
import '../datasources/movie_datasource.dart';

final class MovieRepositoryImpl implements MovieRepository {
  final MovieDatasource _movieDatasource;
  MovieRepositoryImpl({
   required MovieDatasource movieDatasource,
  }) :_movieDatasource = movieDatasource;
  @override
  Future<Either<Failure, List<Movie>>> getMovies() async{
  try {
    final result = await _movieDatasource.getMovies();
    return Right(result);
  } catch (e) {
    return left(ServerFailure(message: e.toString()));
    
  }
  }
  
  @override
  Future<Either<Failure, bool>> createComment(MovieReview movie) async{
   try {
    await _movieDatasource.createReview(MovieReviewModel(body: movie.body, id: movie.id, title: movie.title, user: movie.user, rating: movie.rating, userReviewerId: movie.userReviewerId));
    return const Right(true);
  } catch (e) {
    return left(ServerFailure(message: e.toString()));
    
  }
  }
  
  @override
  Future<Either<Failure, bool>> editComment(MovieReview movie) async{
   try {
     await _movieDatasource.editReview(MovieReviewModel(body: movie.body, id: movie.id, title: movie.title, user: movie.user, rating: movie.rating, userReviewerId: movie.userReviewerId));
    return const Right(true);
  } catch (e) {
    return left(ServerFailure(message: e.toString()));
    
  }
  }
}
