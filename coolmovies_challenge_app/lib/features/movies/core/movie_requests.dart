final class MovieRequests{
  static const String allMoviesPath = r"""
          query AllMovies {
            allMovies(orderBy: RELEASE_DATE_DESC) {
              nodes {
                id
                imgUrl
                userCreatorId
                title
                releaseDate
                nodeId
                userByUserCreatorId {
                  id
                  name
                  nodeId
                }
                movieReviewsByMovieId(orderBy: RATING_DESC) {
        nodes {
          id
          body
          movieId
          title
          rating
          userReviewerId
          userByUserReviewerId {
            id
            name
          }
        }
      } 
              }
            }
          }
        """;

static const String createMovieReviewMutation = r'''
mutation CreateReview($body: String = "", $title: String = "", $rating: Int = 3, $userReviewerId: UUID = "5f1e6707-7c3a-4acd-b11f-fd96096abd5a", $movieId: UUID = "70351289-8756-4101-bf9a-37fc8c7a82cd") {
  createMovieReview(
    input: {movieReview: {title: $title, movieId: $movieId, userReviewerId: $userReviewerId, body: $body, rating: $rating}}
  ) {
    clientMutationId
    movieReview {
      body
      id
      rating
      title
    }
  }
}
''';



static String updateMovieReviewPath  = r''' 
mutation MyMutation($body: String = "", $rating: Int = 3, $title: String = "", $id: UUID = "0b960d7a-9267-4dc4-aa38-61e1e858b731") {
      updateMovieReviewById(
        input: {movieReviewPatch: {body: $body, rating: $rating, title: $title}, id: $id}
      ) {
        clientMutationId
      }
    }
''';
}