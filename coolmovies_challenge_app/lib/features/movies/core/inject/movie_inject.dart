import 'package:coolmovies_challenge/features/movies/data/datasources/movie_datasource.dart';
import 'package:coolmovies_challenge/features/movies/data/datasources/movie_datasource_impl.dart';
import 'package:coolmovies_challenge/features/movies/domain/repositories/movie_repository.dart';
import 'package:coolmovies_challenge/features/movies/domain/usecases/create_reviewer_movie.dart';
import 'package:coolmovies_challenge/features/movies/domain/usecases/get_movies.dart';
import 'package:core/core.dart';
import 'package:core/external/data/graphql_service.dart';

import '../../data/repositories/movie_repository_impl.dart';
import '../../domain/usecases/update_review_usecase.dart';


class MovieInject {

 static Future<void> initialize()async {
  getIt.registerFactory<MovieDatasource>(() => MovieDatasourceImpl(dataService: getIt<GraphQLService>()));

    getIt.registerFactory<MovieRepository>(() => MovieRepositoryImpl(movieDatasource: getIt.get<MovieDatasource>()));


    getIt.registerFactory<GetMoviesUsecase>(() => GetMoviesUsecase(repository: getIt.get<MovieRepository>()));

        getIt.registerFactory<CreateReviewUseCase>(() => CreateReviewUseCase(repository: getIt.get<MovieRepository>()));
          getIt.registerFactory<UpdateReviewUsecase>(() => UpdateReviewUsecase(repository: getIt.get<MovieRepository>()));



  }
}