import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';
import 'package:core/core.dart';



abstract interface class MovieRepository{
  Future<Either<Failure, List<Movie>>> getMovies();
  Future<Either<Failure,bool>> createComment(MovieReview movie);
  Future<Either<Failure,bool>> editComment(MovieReview movie);

}