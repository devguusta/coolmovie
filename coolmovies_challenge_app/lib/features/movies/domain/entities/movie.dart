
import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';


class Movie {
  final String id;
  final String imageUrl;
  final String title;
  final String releaseDate;
  final List<MovieReview>? moviewReview;
  final User? userCreator;
  
  Movie({
    required this.id,
    required this.imageUrl,
    required this.title,
    required this.releaseDate,

this.moviewReview,
    this.userCreator,
    
  });
  

  @override
  String toString() {
    return 'Movie(id: $id, imageUrl: $imageUrl, title: $title, releaseDate: $releaseDate, moviewReview: $moviewReview, userCreator: $userCreator)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Movie &&
      other.id == id &&
      other.imageUrl == imageUrl &&
      other.title == title &&
      other.releaseDate == releaseDate &&
      other.moviewReview == moviewReview &&
      other.userCreator == userCreator ;
     
  }

  @override
  int get hashCode {
    return id.hashCode ^
      imageUrl.hashCode ^
      title.hashCode ^
      releaseDate.hashCode ^
      moviewReview.hashCode ^
      userCreator.hashCode;
  }


}
