

import 'package:coolmovies_challenge/features/movies/domain/entities/user_reviewer.dart';

class MovieReview {
  final String body;
  final String id;
  final String title;
  final num rating;
  final User user;
  final String userReviewerId;
  MovieReview({
    required this.body,
    required this.id,
    required this.title,
    required this.rating,
    required this.user,
    required this.userReviewerId,
  });

  @override
  String toString() {
    return 'MovieReview(body: $body, id: $id, title: $title, rating: $rating, user: $user, userReviewerId: $userReviewerId)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is MovieReview &&
      other.body == body &&
      other.id == id &&
      other.title == title &&
      other.rating == rating &&
      other.user == user &&
      other.userReviewerId == userReviewerId;
  }

  @override
  int get hashCode {
    return body.hashCode ^
      id.hashCode ^
      title.hashCode ^
      rating.hashCode ^
      user.hashCode ^
      userReviewerId.hashCode;
  }

}
