import 'package:core/core.dart';

import '../entities/movie_review.dart';
import '../repositories/movie_repository.dart';

class UpdateReviewUsecase implements UseCase<bool,MovieReview> {
  final MovieRepository repository;
  UpdateReviewUsecase({
    required this.repository,
  });
  @override
  Future<Either<Failure, bool>> call(MovieReview movie) async{
    return await repository.editComment(movie);
  }
}
