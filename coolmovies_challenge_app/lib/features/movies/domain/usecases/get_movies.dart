import 'package:coolmovies_challenge/features/movies/domain/entities/movie.dart';
import 'package:coolmovies_challenge/features/movies/domain/repositories/movie_repository.dart';

import 'package:core/core.dart';

class GetMoviesUsecase implements UseCase {
  final MovieRepository repository;
  GetMoviesUsecase({
    required this.repository,
  });
  @override
  Future<Either<Failure, List<Movie>>> call(_) async{
    return await repository.getMovies();
  }
}
