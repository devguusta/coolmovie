import 'package:coolmovies_challenge/features/movies/domain/entities/entities.dart';
import 'package:core/core.dart';

import '../repositories/movie_repository.dart';

class CreateReviewUseCase implements UseCase<bool,MovieReview> {
  final MovieRepository repository;
  CreateReviewUseCase({
    required this.repository,
  });
  @override
  Future<Either<Failure, bool>> call(MovieReview movie) async{
    return await repository.createComment(movie);
  }
}
