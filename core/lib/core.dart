library core;

export './inject/inject.dart';
export './external/external.dart';



//packages

export 'package:dartz/dartz.dart' hide State;
export 'package:get_it/get_it.dart';
export 'package:equatable/equatable.dart';

export 'package:flutter_bloc/flutter_bloc.dart';
export 'package:bloc_test/bloc_test.dart';
export 'package:flutter_carousel_widget/flutter_carousel_widget.dart';