import 'package:core/external/data/data_inject.dart';


import 'get_it_instance.dart';

class CoreInject {

 static Future<void> initialize() async{
 await  DataInject.inject(getIt);
  }
}