
import 'package:core/external/data/graphql_service.dart';
import 'package:get_it/get_it.dart';


final class DataInject{
  static Future<void> inject(GetIt getIt)async{
    getIt.registerSingleton<GraphQLService>(GraphQLService());
  }
}