import 'dart:io';

import 'package:core/external/data/data_service.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class GraphQLService implements DataService{
  late GraphQLClient _client;

  GraphQLService(){
    init();
  }
  

  Future<GraphQLService> init() async {

    await initHiveForFlutter();


      final HttpLink httpLink = HttpLink(
    Platform.isAndroid
      ? 'http://10.0.2.2:5001/graphql'
      : 'http://localhost:5001/graphql',
  );


    final AuthLink authLink = AuthLink(
       getToken: () async => 'Bearer glpat-bvUpofDMSEcsFjQ9R55Y',
    );

    final Link link = authLink.concat(httpLink);

    _client = GraphQLClient(
      link: link,
      defaultPolicies: DefaultPolicies(
        mutate: Policies(
          fetch: FetchPolicy.noCache,
          error: ErrorPolicy.ignore,
        ),
        query: Policies(
          fetch: FetchPolicy.noCache,
          error: ErrorPolicy.ignore,
        ),
      ),
      cache: GraphQLCache(store: InMemoryStore()),
    );

    return this;
  }

  @override
  Future<Map<String, dynamic>> create({
    required String path,
    Map<String, dynamic> params = const {},
  }) async {
    try {
      final options = MutationOptions(
        variables: params,
        document: gql(path),
      );

      final result = await _client.mutate(options);
      if (result.hasException) {
        throw result.exception!;
      }
      return result.data ?? {};
    } catch (e) {
      _handleException(e);

      return {};
    }
  }

  @override
  Future<Map<String, dynamic>> read({
    required String path,
    Map<String, dynamic> params = const {},
  }) async {
    try {
      final options = QueryOptions(
        variables: params,
        document: gql(path),
      );

      final result = await _client.query(options);

      if (result.hasException) {
         if (result.hasException) {
        throw result.exception!;
      }
      }

      return result.data ?? {};
    } catch (e) {
      _handleException(e);

      return {};
    }
  }

  @override
  Future<Map<String, dynamic>> update({
    required String path,
    Map<String, dynamic> params = const {},
  }) async {
    try {
      final options = MutationOptions(
        variables: params,
        document: gql(path),
      );

      final result = await _client.mutate(options);

      if (result.hasException) {
        throw result.exception!;
      }

      return result.data ?? {};
    } catch (e) {
      _handleException(e);

      return {};
    }
  }

  @override
  Future<Map<String, dynamic>> delete({
    required String path,
    Map<String, dynamic> params = const {},
  }) async {
    try {
      final options = MutationOptions(
        document: gql(path),
        variables: params,
      );

      final result = await _client.mutate(options);

      if (result.hasException) {
        throw result.exception!;
      }

      return result.data ?? {};
    } catch (e) {
      _handleException(e);

      return {};
    }
  }
}


void _handleException(dynamic e) {
  if (e is OperationException && e.linkException != null) {
    throw Exception(e.linkException.toString());
  }

 

  throw  Exception(e.toString());
}