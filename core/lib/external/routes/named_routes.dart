 sealed class CoolMoviesRoutes {
  static const String splash = '/';
  static const String login = '/login';
  static const String home = '/home';
  static const String detailsMovie = '/movies/details';
   static const String createOrEditReview = '/movies/create_edit_review';

}