abstract class Failure {
  final String message;
  const Failure({required this.message});
}

class ServerFailure extends Failure {
  const ServerFailure({required String message}) : super(message: message);
}

class ConnectionFailure extends Failure {
  const ConnectionFailure({required String message}) : super(message: message);
}