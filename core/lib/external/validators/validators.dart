


sealed class ValidatorUtils {

  static String? requiredField(String? value) {
    if (value == null || value.isEmpty) {
      return "Required field";
    }
    return null;
  }

  static String? requiredFieldEmptyText(String? value) {
    if (value == null || value.isEmpty) {
      return "";
    }
    return null;
  }
}