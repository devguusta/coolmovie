import 'package:flutter/material.dart';

import '../colors/app_colors.dart';
import '../styles/text_styles.dart';

class CustomPrimaryButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  const CustomPrimaryButton({
    Key? key,
    required this.onPressed,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  foregroundColor: Colors.white,
                                  textStyle:  AppTextStyles.bodySmall.copyWith(fontSize: 16),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: BorderSide(width:2, color: AppColors.red)
                                  ),
                                ),
                                onPressed: ()async{
                              
                                  onPressed();
                                },
                                child: Text(text),
                              );
  }
}
