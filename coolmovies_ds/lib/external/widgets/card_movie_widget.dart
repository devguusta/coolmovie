import 'package:flutter/material.dart';

class CardMovieWidget extends StatelessWidget {
  final String imageUrl;
  final Widget? body;
  final Size? size;
  const CardMovieWidget({
    Key? key,
    required this.imageUrl,
    this.size,
    this.body,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
                    children: [
                   
                      Container(
                        height: size?.height ?? MediaQuery.of(context).size.height * 0.55,
                        width: size?.width ?? MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            opacity: 0.8,
                            image: NetworkImage(imageUrl,),
                          fit: BoxFit.fill)
                          ,
                        ),
                      ),
                   if(body  != null)...[
                    body!
                   ]
                    
                    ],
                  );
  }
}
