import 'package:flutter/material.dart';

import 'package:core/core.dart';

class CardMoviesCarousel extends StatelessWidget {
  final List movies;
 
  const CardMoviesCarousel({
    Key? key,
    required this.movies,
 
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  ExpandableCarousel(
  options: CarouselOptions(
    autoPlay: true,
    autoPlayInterval: const Duration(seconds: 2),
                 height: MediaQuery.of(context).size.height * 0.4,
                 enlargeCenterPage: true

  ),
  items: movies.map((i) {
    return Builder(
      builder: (BuildContext context) {
        return GestureDetector(
          onTap: (){
            Navigator.pushNamed(context, CoolMoviesRoutes.detailsMovie, arguments: i);
          },
          child: Container(
               height: MediaQuery.of(context).size.height * 0.4,
                          width: MediaQuery.of(context).size.width * 0.6,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              opacity: 0.9,
                              image: NetworkImage(i.imageUrl,),
                            fit: BoxFit.fill)
                            ,
                          ),
          ),
        );
      },
    );
  }).toList(),
                      );
  }
}
