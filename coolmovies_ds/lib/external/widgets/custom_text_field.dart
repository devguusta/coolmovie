import 'package:coolmovies_ds/external/colors/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef StringCallBack = void Function(String value);

class CustomTextFormField extends StatelessWidget {
  final String hintText;
  final TextEditingController? controller;
  final bool obscureText;
  final TextInputType? textInputType;
  final FocusNode? focusNode;
  final StringCallBack? onChanged;
  final String? Function(String? value)? validator;
  final AutovalidateMode? autovalidateMode;
  final TextInputAction? textInputAction;
  final List<TextInputFormatter>? inputFormatters;
  final StringCallBack? onSubmited;
  const CustomTextFormField({
    Key? key,
    required this.hintText,
    this.controller,
    this.obscureText = false,
    this.textInputType,
    this.focusNode,
    this.onChanged,
    this.validator,
    this.autovalidateMode = AutovalidateMode.always,
    this.textInputAction,
    this.inputFormatters,
    this.onSubmited,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      inputFormatters: inputFormatters,
      textInputAction: textInputAction,
      autovalidateMode: autovalidateMode,
      validator: validator,
      controller: controller,
      obscureText: obscureText,
      focusNode: focusNode,
      keyboardType: textInputType,
      onFieldSubmitted: onSubmited,
      textAlign: TextAlign.justify,
      style: const TextStyle(color: Colors.white),
      decoration: InputDecoration(
          contentPadding:
              const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          filled: true,
          fillColor:  AppColors.grey,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide.none,
          ),
          hintText: hintText,
          hintStyle: const TextStyle(
            color: Colors.white,
          ),
          errorStyle: const TextStyle(color: Colors.red)),
      onChanged: onChanged,
    );
  }
}