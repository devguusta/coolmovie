import 'package:flutter/material.dart';
class ShimmerLoading extends StatefulWidget {
  const ShimmerLoading({
    super.key,
    
    required this.child,
  });

  
  final Widget child;

  @override
  State<ShimmerLoading> createState() => _ShimmerLoadingState();
}
var  _shimmerGradient = LinearGradient(
  colors: [
 Colors.black.withOpacity(0.8),
     Colors.black.withOpacity(0.8),
    const Color(0xFFF4F4F4),
    Colors.black.withOpacity(0.8),
        Colors.black.withOpacity(0.8),

  ],
  stops: const <double>[
              0.0,
              0.35,
              0.5,
              0.65,
              1.0
            ],
  begin: Alignment.topLeft,
            end: Alignment.centerRight,
  tileMode: TileMode.clamp,
);
class _ShimmerLoadingState extends State<ShimmerLoading> {
  
  @override
  Widget build(BuildContext context) {
  

    return ShaderMask(
      blendMode: BlendMode.srcATop,
      shaderCallback: (bounds) {
        return _shimmerGradient.createShader(bounds);
      },
      child: widget.child,
    );
  }
}