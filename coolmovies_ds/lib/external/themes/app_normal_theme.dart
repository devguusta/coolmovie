import 'package:coolmovies_ds/external/colors/app_colors.dart';
import 'package:flutter/material.dart';

class AppNormalTheme {
static ThemeData theme =   ThemeData(
        
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
        scaffoldBackgroundColor: AppColors.black,
        appBarTheme: AppBarTheme(
          backgroundColor: AppColors.black,
          iconTheme: IconThemeData(
            color: AppColors.white,
          )
          
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: AppColors.black
          
        )
      );
}