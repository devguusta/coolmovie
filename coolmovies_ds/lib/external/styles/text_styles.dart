import 'package:coolmovies_ds/external/external.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

final class AppTextStyles {

static    final TextStyle title = GoogleFonts.inter(
    fontWeight: FontWeight.w800,
    fontSize: 32,
    color: AppColors.white,
  );
static   final TextStyle bodyLarge = GoogleFonts.inter(
    fontWeight: FontWeight.w600,
    fontSize: 24,
    color: AppColors.white,
  );

    static final TextStyle bodyMedium = GoogleFonts.inter(
    fontWeight: FontWeight.w700,
    fontSize: 20,
    color: AppColors.white,
  );
  static  final TextStyle bodySmall = GoogleFonts.inter(
   
    fontSize: 15,
    color: AppColors.white,
  );

}