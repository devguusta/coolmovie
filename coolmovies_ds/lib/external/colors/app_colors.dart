

import 'package:flutter/material.dart';

sealed class AppColors {
  static Color red = const Color(0xffEB3223);
  static Color black = Colors.black45;
  static Color grey = const Color(0xff383737);
  static Color white = const Color(0xffFFFFFF);

}